#include "Juego.h"



bool Juego::Init()
{
	bool success = true; 
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		std::cout << "SDL could not initialize! SDL_Error: " << SDL_GetError() << std::endl;
		success = false;
	}
	else
	{
		window = SDL_CreateWindow("Programacion de Graficos", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);

		if (window == NULL)
		{
			std::cout << "Window could not be created! SDL_Error: " << SDL_GetError() << std::endl;
			success = false;
		}
		else
		{
			screenSurface = SDL_GetWindowSurface(window);

			if (screenSurface == NULL)
			{
				std::cout << "Screen surface could not be created! SDL_Error: " << SDL_GetError() << std::endl;
				success = false;
			}
			imageSurface = loadSurface("yeah.bmp");
		}
	}
	return success;
}

void Juego::Close()
{
	SDL_FreeSurface(screenSurface);
	SDL_FreeSurface(imageSurface);
	SDL_DestroyWindow(window);
	SDL_Quit();
}

SDL_Surface* Juego::loadSurface(std::string path)
{
	//Load image at specified path
	SDL_Surface* loadedSurface = SDL_LoadBMP(path.c_str());
	if (loadedSurface == NULL)
	{
		printf("Unable to load image %s! SDL Error: %s\n", path.c_str(), SDL_GetError());
	}

	return loadedSurface;
}

void Juego::Run()
{
	if (!Init())
	{
		std::cout << "There was a problem starting the game!" << std::endl;
	}
	else
	{
		bool run = true;
		while (run)
		{
			while (SDL_PollEvent(&windowEvent) != 0)
			{
				if (SDL_PollEvent(&windowEvent))
				{
					if (SDL_QUIT == windowEvent.type)
					{
						run = false;
					}				
				}
			}

			SDL_BlitSurface(imageSurface, NULL, screenSurface, NULL);
			//Update the surface
			SDL_UpdateWindowSurface(window);
		}
		Close();
	}
}

Juego::Juego()
{

}

Juego::~Juego()
{
}
