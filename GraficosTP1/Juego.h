#pragma once
#include <SDL.h>
#include <iostream>
class Juego
{
private:
	const int SCREEN_WIDTH = 800;
	const int SCREEN_HEIGHT = 600;
	
	SDL_Window* window = NULL;
	SDL_Surface* screenSurface = NULL;
	SDL_Surface* imageSurface = NULL;
	SDL_Surface* loadSurface(std::string path);
	SDL_Event windowEvent;

	bool Init();
	void Close();
public:
	Juego();
	~Juego();
	void Run();
};

